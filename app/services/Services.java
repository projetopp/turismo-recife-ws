package services;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import models.Feira;
import models.Mercado;
import models.Museu;
import models.Ponte;
import models.Shopping;
import models.Teatro;
import models.comentarios.FeiraComentario;
import models.comentarios.MercadoComentario;
import models.comentarios.MuseuComentario;
import models.comentarios.PonteComentario;
import models.comentarios.ShoppingComentario;
import models.comentarios.TeatroComentario;
import play.Logger;
import play.db.jpa.JPA;

@SuppressWarnings("all")
public class Services {
	public static Services services;
	private Services(){
		
	}
	
	public static Services getService(){
		services = new Services();
		return services;
	}
	
	public List<Mercado> getMercados(){
		List<Mercado> mercados = JPA.em().createQuery("FROM Mercado").getResultList();
        if(mercados.size()==0){
        	mercados = inserirMercados();
        }
		return mercados;
	}
	
	public void salvarComentarioMercado(MercadoComentario comentario){
		JPA.em().persist(comentario);
	}
	
	public List<MercadoComentario> listarComentariosMercado(int idMercado){
		List<MercadoComentario> comentarios = JPA.em()
				.createQuery("FROM MercadoComentario as m WHERE m.mercado.id = " + idMercado).getResultList();
		
		return comentarios;
	}

	private List<Mercado> inserirMercados() {
		
		String arquivoCSV = "app/csv/mercadospublicos.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = "\n";
		try {

		    br = new BufferedReader(new FileReader(arquivoCSV));
		    while ((linha = br.readLine()) != null) {

		        String[] linhas = linha.split(csvDivisor);

		        for (String linhaStr : linhas) {
					String[] dados = linhaStr.split(";");
					Mercado mercado = new Mercado();
					mercado.setNome(dados[0]);
					mercado.setDescricao(dados[1]);
					mercado.setBairro(dados[2]);
					mercado.setLatitude(dados[3]);
					mercado.setLongitude(dados[4]);
					
					JPA.em().persist(mercado);
				}

		    }

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Logger.info("Erro File");
		} catch (IOException e) {
		    e.printStackTrace();
		    Logger.info("Erro IO File");
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return JPA.em().createQuery("FROM Mercado").getResultList();
	}
	
	public List<Museu> getMuseus(){
		List<Museu> museus = JPA.em().createQuery("FROM Museu").getResultList();
        if(museus.size()==0){
        	museus = inserirMuseus();
        }
		return museus;
	}
	
	public void salvarComentarioMuseu(MuseuComentario comentario){
		JPA.em().persist(comentario);
	}
	
	public List<MuseuComentario> listarComentariosMuseu(int idMuseu){
		List<MuseuComentario> comentarios = JPA.em()
				.createQuery("FROM MuseuComentario as m WHERE m.museu.id = " + idMuseu).getResultList();
		
		return comentarios;
	}
	
	private List<Museu> inserirMuseus() {
		
		String arquivoCSV = "app/csv/museu1.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = "\n";
		try {

		    br = new BufferedReader(new FileReader(arquivoCSV));
		    while ((linha = br.readLine()) != null) {

		        String[] linhas = linha.split(csvDivisor);

		        for (String linhaStr : linhas) {
					String[] dados = linhaStr.split(";");
					Museu museu = new Museu();
					museu.setNome(dados[0]);
					museu.setDescricao(dados[1]);
					museu.setBairro(dados[2]);
					museu.setLogradouro(dados[3]);
					museu.setLatitude(dados[4]);
					museu.setLongitude(dados[5]);
					museu.setTelefone(dados[6]);
					museu.setSite(dados[7]);
					
					JPA.em().persist(museu);
				}

		    }

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Logger.info("Erro File");
		} catch (IOException e) {
		    e.printStackTrace();
		    Logger.info("Erro IO File");
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return JPA.em().createQuery("FROM Museu").getResultList();
	}

	public List<Teatro> getTeatros(){
		List<Teatro> teatros = JPA.em().createQuery("FROM Teatro").getResultList();
	    if(teatros.size()==0){
	    	teatros = inserirTeatros();
	    }
		return teatros;
	}
	
	public void salvarComentarioTeatro(TeatroComentario comentario){
		JPA.em().persist(comentario);
	}
	
	public List<TeatroComentario> listarComentariosTeatro(int idTeatro){
		List<TeatroComentario> comentarios = JPA.em()
				.createQuery("FROM TeatroComentario as t WHERE t.teatro.id = " + idTeatro).getResultList();
		
		return comentarios;
	}
	
	private List<Teatro> inserirTeatros() {
		
		String arquivoCSV = "app/csv/teatros.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = "\n";
		try {

		    br = new BufferedReader(new FileReader(arquivoCSV));
		    while ((linha = br.readLine()) != null) {

		        String[] linhas = linha.split(csvDivisor);

		        for (String linhaStr : linhas) {
					String[] dados = linhaStr.split(";");
					Teatro teatro = new Teatro();
					teatro.setNome(dados[0]);
					teatro.setDescricao(dados[1]);
					teatro.setBairro(dados[2]);
					teatro.setLogradouro(dados[3]);
					teatro.setTelefone(dados[4]);
					teatro.setLatitude(dados[5]);
					teatro.setLongitude(dados[6]);
					JPA.em().persist(teatro);
				}

		    }

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Logger.info("Erro File");
		} catch (IOException e) {
		    e.printStackTrace();
		    Logger.info("Erro IO File");
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return JPA.em().createQuery("FROM Teatro").getResultList();
	}
	
	public List<Feira> getFeiras(){
		List<Feira> feiras = JPA.em().createQuery("FROM Feira").getResultList();
	    if(feiras.size()==0){
	    	feiras = inserirFeiras();
	    }
		return feiras;
	}
	
	public void salvarComentarioFeira(FeiraComentario comentario){
		JPA.em().persist(comentario);
	}
	
	public List<FeiraComentario> listarComentariosFeira(int idFeira){
		List<FeiraComentario> comentarios = JPA.em()
				.createQuery("FROM FeiraComentario as f WHERE f.feira.id = " + idFeira).getResultList();
		
		return comentarios;
	}
	
	private List<Feira> inserirFeiras() {
		
		String arquivoCSV = "app/csv/feiraslivres.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = "\n";
		try {

		    br = new BufferedReader(new FileReader(arquivoCSV));
		    while ((linha = br.readLine()) != null) {

		        String[] linhas = linha.split(csvDivisor);

		        for (String linhaStr : linhas) {
					String[] dados = linhaStr.split(";");
					Feira feira = new Feira();
					feira.setNome(dados[0]);
					feira.setLocalizacao(dados[1]);
					feira.setDiasAbertos(dados[2]);
					feira.setHorario(dados[3]);
					feira.setObservacao(dados[4]);
					feira.setLatitude(dados[5]);
					feira.setLongitude(dados[6]);
					JPA.em().persist(feira);
				}

		    }

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Logger.info("Erro File");
		} catch (IOException e) {
		    e.printStackTrace();
		    Logger.info("Erro IO File");
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return JPA.em().createQuery("FROM Feira").getResultList();
	}
	
	public List<Ponte> getPontes(){
		List<Ponte> pontes = JPA.em().createQuery("FROM Ponte").getResultList();
	    if(pontes.size()==0){
	    	pontes = inserirPontes();
	    }
		return pontes;
	}
	
	public void salvarComentarioPonte(PonteComentario comentario){
		JPA.em().persist(comentario);
	}
	
	public List<PonteComentario> listarComentariosPonte(int idPonte){
		List<PonteComentario> comentarios = JPA.em()
				.createQuery("FROM PonteComentario as p WHERE p.ponte.id = " + idPonte).getResultList();
		
		return comentarios;
	}
	
	private List<Ponte> inserirPontes() {
		
		String arquivoCSV = "app/csv/pontesdorecife.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = "\n";
		try {

		    br = new BufferedReader(new FileReader(arquivoCSV));
		    while ((linha = br.readLine()) != null) {

		        String[] linhas = linha.split(csvDivisor);

		        for (String linhaStr : linhas) {
					String[] dados = linhaStr.split(";");
					Ponte ponte = new Ponte();
					ponte.setNome(dados[0]);
					ponte.setDescricao(dados[1]);
					ponte.setBairro(dados[2]);
					ponte.setLatitude(dados[3]);
					ponte.setLongitude(dados[4]);
					JPA.em().persist(ponte);
				}

		    }

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Logger.info("Erro File");
		} catch (IOException e) {
		    e.printStackTrace();
		    Logger.info("Erro IO File");
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return JPA.em().createQuery("FROM Ponte").getResultList();
	}
	
	public List<Shopping> getShoppings(){
		List<Shopping> shoppings = JPA.em().createQuery("FROM Shopping").getResultList();
	    if(shoppings.size()==0){
	    	shoppings = inserirShoppings();
	    }
		return shoppings;
	}
	
	public void salvarComentarioShopping(ShoppingComentario comentario){
		JPA.em().persist(comentario);
	}
	
	public List<ShoppingComentario> listarComentariosShopping(int idShopping){
		List<ShoppingComentario> comentarios = JPA.em()
				.createQuery("FROM ShoppingComentario as s WHERE s.shopping.id = " + idShopping).getResultList();
		
		return comentarios;
	}
	
	private List<Shopping> inserirShoppings() {
		
		String arquivoCSV = "app/csv/shopping.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = "\n";
		try {

		    br = new BufferedReader(new FileReader(arquivoCSV));
		    while ((linha = br.readLine()) != null) {

		        String[] linhas = linha.split(csvDivisor);

		        for (String linhaStr : linhas) {
					String[] dados = linhaStr.split(";");
					Shopping shopping = new Shopping();
					shopping.setNome(dados[0]);
					shopping.setDescricao(dados[1]);
					shopping.setBairro(dados[2]);
					shopping.setLogradouro(dados[3]);
					shopping.setLatitude(dados[4]);
					shopping.setLongitude(dados[5]);
					shopping.setTelefone(dados[6]);
					shopping.setSite(dados[7]);
					shopping.setFuncionamento(dados[8]);
					shopping.setFuncionamentoDomingo(dados[9]);
					JPA.em().persist(shopping);
				}

		    }

		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		    Logger.info("Erro File");
		} catch (IOException e) {
		    e.printStackTrace();
		    Logger.info("Erro IO File");
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		return JPA.em().createQuery("FROM Shopping").getResultList();
	}

}
