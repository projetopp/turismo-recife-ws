package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import models.comentarios.ShoppingComentario;

@Entity
@Table(name = "shoppings")
public class Shopping {
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private int id;
		private String nome;
		@Column(length = 1000)
		private String descricao;
		private String bairro;
		private String logradouro;
		private String latitude;
		private String longitude;
		private String telefone;
		private String site;
		private String funcionamento;
		private String funcionamentoDomingo;
		@OneToMany(mappedBy = "shopping", targetEntity = ShoppingComentario.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
		private List<ShoppingComentario> comentarios = new ArrayList<>();;
		
		@JsonIgnore
		public List<ShoppingComentario> getComentarios() {
			return comentarios;
		}
		public void setComentarios(List<ShoppingComentario> comentarios) {
			this.comentarios = comentarios;
		}
		public int getId() {
			return this.id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNome() {
			return this.nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getDescricao() {
			return this.descricao;
		}
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		public String getBairro() {
			return this.bairro;
		}
		public void setBairro(String bairro) {
			this.bairro = bairro;
		}
		public String getLogradouro() {
			return this.logradouro;
		}
		public void setLogradouro(String logradouro) {
			this.logradouro = logradouro;
		}
		public String getLatitude() {
			return this.latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getLongitude() {
			return this.longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		public String getTelefone() {
			return this.telefone;
		}
		public void setTelefone(String telefone) {
			this.telefone = telefone;
		}
		public String getSite() {
			return this.site;
		}
		public void setSite(String site) {
			this.site = site;
		}
		public String getFuncionamento() {
			return this.funcionamento;
		}
		public void setFuncionamento(String funcionamento) {
			this.funcionamento = funcionamento;
		}
		public String getFuncionamentoDomingo() {
			return this.funcionamentoDomingo;
		}
		public void setFuncionamentoDomingo(String funcionamentoDomingo) {
			this.funcionamentoDomingo = funcionamentoDomingo;
		}
		
		
		
}
