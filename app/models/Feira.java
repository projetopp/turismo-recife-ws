package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import models.comentarios.FeiraComentario;

@Entity
@Table(name = "feiras")
public class Feira {
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private int id;
		private String nome;
		private String localizacao;
		private String diasAbertos;
		private String observacao;
		private String horario;
		private String latitude;
		private String longitude;
		@OneToMany(mappedBy = "feira", targetEntity = FeiraComentario.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
		private List<FeiraComentario> comentarios = new ArrayList<>();
		
		@JsonIgnore
		public List<FeiraComentario> getComentarios() {
			return comentarios;
		}
		public void setComentarios(List<FeiraComentario> comentarios) {
			this.comentarios = comentarios;
		}
		public int getId() {
			return this.id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNome() {
			return this.nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public String getLatitude() {
			return this.latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getLongitude() {
			return this.longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		public String getLocalizacao() {
			return this.localizacao;
		}
		public void setLocalizacao(String localizacao) {
			this.localizacao = localizacao;
		}
		public String getDiasAbertos() {
			return this.diasAbertos;
		}
		public void setDiasAbertos(String diasAbertos) {
			this.diasAbertos = diasAbertos;
		}
		public String getObservacao() {
			return this.observacao;
		}
		public void setObservacao(String observacao) {
			this.observacao = observacao;
		}
		public String getHorario() {
			return this.horario;
		}
		public void setHorario(String horario) {
			this.horario = horario;
		}

}
