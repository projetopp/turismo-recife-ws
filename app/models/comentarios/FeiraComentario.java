package models.comentarios;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.Feira;

@Entity
@Table(name = "feira_comentario")
public class FeiraComentario {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private Date data;
	private String nomeDaPessoa;
	private String comentario;
	@ManyToOne
	@JoinColumn(name="feira_id", nullable = false, updatable = false, insertable = true)
	private Feira feira;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getNomeDaPessoa() {
		return nomeDaPessoa;
	}
	public void setNomeDaPessoa(String nomeDaPessoa) {
		this.nomeDaPessoa = nomeDaPessoa;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Feira getFeira() {
		return feira;
	}
	public void setFeira(Feira feira) {
		this.feira = feira;
	}
	
	

}
