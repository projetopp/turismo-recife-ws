package models.comentarios;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.Shopping;

@Entity
@Table(name = "shopping_comentario")
public class ShoppingComentario {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private Date data;
	private String nomeDaPessoa;
	private String comentario;
	@ManyToOne
	@JoinColumn(name="shopping_id", nullable = false, updatable = false, insertable = true)
	private Shopping shopping;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getNomeDaPessoa() {
		return nomeDaPessoa;
	}
	public void setNomeDaPessoa(String nomeDaPessoa) {
		this.nomeDaPessoa = nomeDaPessoa;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Shopping getShopping() {
		return shopping;
	}
	public void setShopping(Shopping shopping) {
		this.shopping = shopping;
	}
}
