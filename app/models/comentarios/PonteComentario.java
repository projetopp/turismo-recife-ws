package models.comentarios;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.Ponte;

@Entity
@Table(name = "ponte_comentario")
public class PonteComentario {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private Date data;
	private String nomeDaPessoa;
	private String comentario;
	@ManyToOne
	@JoinColumn(name="ponte_id", nullable = false, updatable = false, insertable = true)
	private Ponte ponte;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getNomeDaPessoa() {
		return nomeDaPessoa;
	}
	public void setNomeDaPessoa(String nomeDaPessoa) {
		this.nomeDaPessoa = nomeDaPessoa;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Ponte getPonte() {
		return ponte;
	}
	public void setPonte(Ponte ponte) {
		this.ponte = ponte;
	}
}
