package controllers;


import java.util.Date;
import java.util.List;

import models.Feira;
import models.Mercado;
import models.Museu;
import models.Ponte;
import models.Shopping;
import models.Teatro;
import models.comentarios.FeiraComentario;
import models.comentarios.MercadoComentario;
import models.comentarios.MuseuComentario;
import models.comentarios.PonteComentario;
import models.comentarios.ShoppingComentario;
import models.comentarios.TeatroComentario;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.Services;
import views.html.index;


public class Application extends Controller {
	private static Services service = Services.getService();
  
    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
    
    @Transactional
    public static Result mercados() {
    	List<Mercado> mercados = service.getMercados();
        return ok(Json.toJson(mercados));
    }
    
    @Transactional
    public static Result inserirComentarioMercado(){
    	
    	DynamicForm form = Form.form().bindFromRequest();
    	
    	if (form.data().size() == 0) {
    		return badRequest("Nenhuma informação foi enviada.");
    	}
    	
    	Mercado mercado = JPA.em().find(Mercado.class, Integer.parseInt(form.get("mercadoId")));
    	
    	MercadoComentario comentario = new MercadoComentario();
    	comentario.setMercado(mercado);
    	comentario.setData(new Date());
    	comentario.setNomeDaPessoa(form.get("nome"));
    	comentario.setComentario(form.get("comentario"));
    	
    	service.salvarComentarioMercado(comentario);
    	
    	return ok("Comentário enviado.");
    }
    
    @Transactional
    public static Result listarComentariosMercado(int idMercado){
    	List<MercadoComentario> comentarios = service.listarComentariosMercado(idMercado);
        return ok(Json.toJson(comentarios));
    }
    
    @Transactional
    public static Result museus() {
    	List<Museu> museus = service.getMuseus();
        return ok(Json.toJson(museus));
    }
    
    @Transactional
    public static Result inserirComentarioMuseu(){
    	
    	DynamicForm form = Form.form().bindFromRequest();
    	
    	if (form.data().size() == 0) {
    		return badRequest("Nenhuma informação foi enviada.");
    	}
    	
    	Museu museu = JPA.em().find(Museu.class, Integer.parseInt(form.get("museuId")));
    	
    	MuseuComentario comentario = new MuseuComentario();
    	comentario.setMuseu(museu);
    	comentario.setData(new Date());
    	comentario.setNomeDaPessoa(form.get("nome"));
    	comentario.setComentario(form.get("comentario"));
    	
    	service.salvarComentarioMuseu(comentario);
    	
    	return ok("Comentário enviado.");
    }
    
    @Transactional
    public static Result listarComentariosMuseu(int idMuseu){
    	List<MuseuComentario> comentarios = service.listarComentariosMuseu(idMuseu);
        return ok(Json.toJson(comentarios));
    }
    
    @Transactional
    public static Result teatros() {
    	List<Teatro> teatros = service.getTeatros();
        return ok(Json.toJson(teatros));
    }
    
    @Transactional
    public static Result inserirComentarioTeatro(){
    	
    	DynamicForm form = Form.form().bindFromRequest();
    	
    	if (form.data().size() == 0) {
    		return badRequest("Nenhuma informação foi enviada.");
    	}
    	
    	Teatro teatro = JPA.em().find(Teatro.class, Integer.parseInt(form.get("teatroId")));
    	
    	TeatroComentario comentario = new TeatroComentario();
    	comentario.setTeatro(teatro);
    	comentario.setData(new Date());
    	comentario.setNomeDaPessoa(form.get("nome"));
    	comentario.setComentario(form.get("comentario"));
    	
    	service.salvarComentarioTeatro(comentario);
    	
    	return ok("Comentário enviado.");
    }
    
    @Transactional
    public static Result listarComentariosTeatro(int idTeatro){
    	List<TeatroComentario> comentarios = service.listarComentariosTeatro(idTeatro);
        return ok(Json.toJson(comentarios));
    }
    
    @Transactional
    public static Result feiras() {
    	List<Feira> feiras = service.getFeiras();
        return ok(Json.toJson(feiras));
    }
    
    @Transactional
    public static Result inserirComentarioFeira(){
    	
    	DynamicForm form = Form.form().bindFromRequest();
    	
    	if (form.data().size() == 0) {
    		return badRequest("Nenhuma informação foi enviada.");
    	}
    	
    	Feira feira = JPA.em().find(Feira.class, Integer.parseInt(form.get("feiraId")));
    	
    	FeiraComentario comentario = new FeiraComentario();
    	comentario.setFeira(feira);
    	comentario.setData(new Date());
    	comentario.setNomeDaPessoa(form.get("nome"));
    	comentario.setComentario(form.get("comentario"));
    	
    	service.salvarComentarioFeira(comentario);
    	
    	return ok("Comentário enviado.");
    }
    
    @Transactional
    public static Result listarComentariosFeira(int idFeira){
    	List<FeiraComentario> comentarios = service.listarComentariosFeira(idFeira);
        return ok(Json.toJson(comentarios));
    }
    
    @Transactional
    public static Result pontes() {
    	List<Ponte> pontes = service.getPontes();
        return ok(Json.toJson(pontes));
    }
    
    @Transactional
    public static Result inserirComentarioPonte(){
    	
    	DynamicForm form = Form.form().bindFromRequest();
    	
    	if (form.data().size() == 0) {
    		return badRequest("Nenhuma informação foi enviada.");
    	}
    	
    	Ponte ponte = JPA.em().find(Ponte.class, Integer.parseInt(form.get("ponteId")));
    	
    	PonteComentario comentario = new PonteComentario();
    	comentario.setPonte(ponte);
    	comentario.setData(new Date());
    	comentario.setNomeDaPessoa(form.get("nome"));
    	comentario.setComentario(form.get("comentario"));
    	
    	service.salvarComentarioPonte(comentario);
    	
    	return ok("Comentário enviado.");
    }
    
    @Transactional
    public static Result listarComentariosPonte(int idPonte){
    	List<PonteComentario> comentarios = service.listarComentariosPonte(idPonte);
        return ok(Json.toJson(comentarios));
    }
    
    @Transactional
    public static Result shoppings() {
    	List<Shopping> shoppings = service.getShoppings();
        return ok(Json.toJson(shoppings));
    }
    
    @Transactional
    public static Result inserirComentarioShopping(){
    	
    	DynamicForm form = Form.form().bindFromRequest();
    	
    	if (form.data().size() == 0) {
    		return badRequest("Nenhuma informação foi enviada.");
    	}
    	
    	Shopping shopping = JPA.em().find(Shopping.class, Integer.parseInt(form.get("shoppingId")));
    	
    	ShoppingComentario comentario = new ShoppingComentario();
    	comentario.setShopping(shopping);
    	comentario.setData(new Date());
    	comentario.setNomeDaPessoa(form.get("nome"));
    	comentario.setComentario(form.get("comentario"));
    	
    	service.salvarComentarioShopping(comentario);
    	
    	return ok("Comentário enviado.");
    }
    
    @Transactional
    public static Result listarComentariosShopping(int idShopping){
    	List<ShoppingComentario> comentarios = service.listarComentariosShopping(idShopping);
        return ok(Json.toJson(comentarios));
    }
    
    public static Result loginSubmit(){
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        Logger.info("Username is: " + dynamicForm.get("username"));
        Logger.info("Password is: " + dynamicForm.get("password"));
        return ok("ok, I recived POST data. That's all...");
        
    }
  
}
